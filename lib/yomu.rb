require 'yomu/version'

require 'net/http'

require 'socket'
require 'stringio'

class Yomu
  GEMPATH = File.dirname(File.dirname(__FILE__))
  JARPATH = File.join(Yomu::GEMPATH, 'jar', 'tika-app-2.9.0.jar')
  CONFPATH = File.join(Yomu::GEMPATH, 'tika-config.xml')
  DEFAULT_SERVER_PORT = 9293 # an arbitrary, but perfectly cromulent, port

  # Read text or metadata from a data buffer.
  #
  #   data = File.read 'sample.pages'
  #   text = Yomu.read :text, data
  #   metadata = Yomu.read :metadata, data
  def self.read(type, data)
    result = self._client_read(type, data)

    case type
    when :text
      result
    when :html
      result
    when :metadata
      JSON.parse(result)
    end
  end

  def self._client_read(type, data)
    switch = case type
             when :text
               '-t'
             when :html
               '-h'
             when :metadata
               '-m -j'
             end

    IO.popen "#{java} -Djava.awt.headless=true -jar #{Yomu::JARPATH} --config=#{Yomu::CONFPATH} #{switch}", 'r+' do |io|
      io.write data
      io.close_write
      io.read
    end
  end

  def self.java
    ENV['JAVA_HOME'] ? ENV['JAVA_HOME'] + '/bin/java' : 'java'
  end

  private_class_method :java
end
