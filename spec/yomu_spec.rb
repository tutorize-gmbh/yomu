require 'helper.rb'
require 'yomu'

describe Yomu do
  let(:data) { File.read 'spec/samples/sample.docx' }

  before do
    ENV['JAVA_HOME'] = nil
  end

  describe '.read' do
    it 'reads text' do
      text = Yomu.read :text, data

      expect(text).to include 'The quick brown fox jumped over the lazy cat.'
    end

    it 'reads metadata' do
      metadata = Yomu.read :metadata, data

      expect(metadata['Content-Type']).to eql 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    end

    it 'reads metadata values with colons as strings' do
      data = File.read 'spec/samples/sample-metadata-values-with-colons.doc'
      metadata = Yomu.read :metadata, data

      expect(metadata['dc:title']).to eql 'problem: test'
    end

    it 'reads mimetype' do
      mimetype = Yomu.read :mimetype, data

      expect(mimetype.content_type).to eql 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      expect(mimetype.extensions).to include 'docx'
    end
  end
end
